#  截图定位方法

该文件夹下的脚本均需要使用截图来进行坐标定位计算。基本方法是，首先最大化游戏窗口，按下键盘截图键，然后打开画图软件，寻找定位点坐标，并修改相关属性。

大多数需要修改的属性都能够在右键脚本`属性`，`自定义界面`中修改。（如果没找到那就只能右键脚本`编辑脚本`修改代码片段了）

## 魔药连连看

定位红色的两个点的坐标。

![魔药连连看](截图示例/魔药连连看.png)
